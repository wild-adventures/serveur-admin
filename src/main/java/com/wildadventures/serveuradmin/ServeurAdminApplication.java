package com.wildadventures.serveuradmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServeurAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServeurAdminApplication.class, args);
	}

}

